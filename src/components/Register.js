import React, { Component } from 'react';
import { Form, Input, Button, Col } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import Api from '../Api';
import Token from '../Token';
import Warning from './Warning';
export default class Register extends Component {
	constructor() {
		super();
		this.state = {
			name: '',
			username: '',
			email: '',
			password: '',
			token: Token,
			msg: '',
			succes: false
		};
	}

	onChange = (e) => this.setState({ [e.target.name]: e.target.value, msg:'' });

	onSubmit = (e) => {
		e.preventDefault();
		const { name, username, email, password } = this.state,
			data = { name, username, email, password };

		Api.post('/user/create', data)
			.then((res) => {
				sessionStorage.setItem('token', JSON.stringify(res.data.token));
				this.setState({ token: res.data.token });
				console.log('res', res.data);
			})
			.catch((err) => {
				this.setState({ msg: err.response.data.message.message });
				console.log('err', err.response.data);
			});
	};

	render() {
		const { token, msg, success } = this.state;
		return (
			<Col sm={12}>
				{token && <Redirect to="/dashboard" />}
				<Form className="form-pages">
					<h1 className="text-center">Register</h1>
					{msg && <Warning color={success ? 'info' : 'danger'} msg={msg} />}
					<Col className="my-3" sm={12}>
						<Input onChange={this.onChange} type="text" name="name" placeholder="your name.." />
					</Col>
					<Col className="my-3" sm={12}>
						<Input onChange={this.onChange} type="text" name="username" placeholder="your username.." />
					</Col>
					<Col className="my-3" sm={12}>
						<Input onChange={this.onChange} type="email" name="email" placeholder="your email.." />
					</Col>
					<Col className="my-3" sm={12}>
						<Input onChange={this.onChange} type="password" name="password" placeholder="your password.." />
					</Col>
					<Col className="my-3" sm={12}>
						<Button type="submit" color="info" className="w-100" onClick={this.onSubmit}>
							Register
						</Button>
					</Col>
					<Col className="my-3" sm={12}>
						<Link to="/">
							<Button outline color="secondary" className="w-100">
								Already Have Account?
							</Button>
						</Link>
					</Col>
				</Form>
			</Col>
		);
	}
}
