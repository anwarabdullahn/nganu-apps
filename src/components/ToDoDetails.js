import React, { Component } from 'react';
import { Card, CardTitle } from 'reactstrap';
import Api from '../Api';
import Token from '../Token';

export default class ToDoDetails extends Component {
	constructor() {
		super();
		this.state = {
			id: '',
			todolist: {}
		};
	}

	componentDidMount() {
		console.log('did months');
		const { id } = this.props;
		const data = { id };
		this.setState({ id: id }, function() {
			Api.post('/user/todoList/read', data, {
				headers: { Authorization: 'Bearer ' + Token }
			})
				.then((res) => {
					console.log('res satu', res.data.todoList);
					this.setState({
						todolist: res.data.todoList
					});
				})
				.catch((err) => console.log(err.response));
		});
	}

	render1() {
		return (
			<Card body className="my-3 bg-warning" onClick={this.toggle}>
				<CardTitle className="text-dark text-center">Tambahkan ToDoList</CardTitle>
			</Card>
		);
	}

	render2() {
		return <div />;
	}
	render() {
		const { id, todoList } = this.props;
		console.log('id', id);
		console.log('todoList', todoList);
		return id ? this.render1() : this.render2();
	}
}
