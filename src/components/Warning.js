import React from 'react';
import { Alert } from 'reactstrap';
const Warning = (props) => {
	console.log(props);
	return (
		<Alert color={props.color} fade={true}>
			{props.msg}
		</Alert>
	);
};
export default Warning;
