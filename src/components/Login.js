import React, { Component } from 'react';
import { Form, Input, Button, Col } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import Api from '../Api';
import Token from '../Token';
import Warning from './Warning';

export default class Login extends Component {
	constructor() {
		super();
		this.state = {
			email: '',
			password: '',
			token: Token,
			msg: '',
			succes: false
		};
	}

	onSubmit = (e) => {
		e.preventDefault();
		const { email, password } = this.state,
			data = { email, password };
		// console.log('data', data);
		Api.post('/user/login', data)
			.then((res) => {
				sessionStorage.setItem('token', JSON.stringify(res.data.token));
				this.setState({ token: res.data.token });
				this.props.history.push('/dashboard');
				console.log(res.data);
			})
			.catch((err) => {
				this.setState({ msg: err.response.data.message });
				console.log('err', err.response);
			});
	};

	handleInput = (e) => this.setState({ [e.target.name]: e.target.value, msg: '' });

	render() {
		const { token, msg, success } = this.state;
		return (
			<Col sm={12}>
				<Form className="form-pages">
					<h1 className="text-center">Login</h1>
					{msg && <Warning color={success ? 'info' : 'danger'} msg={msg} />}
					<Col className="my-3" sm={12}>
						<Input onChange={this.handleInput} type="email" name="email" placeholder="your email.." />
					</Col>
					<Col className="my-3" sm={12}>
						<Input
							onChange={this.handleInput}
							type="password"
							name="password"
							placeholder="your password.."
						/>
					</Col>
					<Col className="my-3" sm={12}>
						<Button type="submit" color="info" className="w-100" onClick={this.onSubmit}>
							Login
						</Button>
					</Col>
					<Col className="my-3" sm={12}>
						<Link to="/register">
							<Button outline color="secondary" className="w-100">
								I don't Have Account?
							</Button>
						</Link>
					</Col>
				</Form>
			</Col>
		);
	}
}
