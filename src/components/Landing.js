import React, { Component } from 'react';
export default class Landing extends Component {
	render() {
		return (
			<div className="text-center">
				<h1 className="display-4">Mini Todo App</h1>
				<p className="display-5">share your todo with your friends now</p>
			</div>
		);
	}
}
