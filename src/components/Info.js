import React, { Component } from 'react';
import { Card, CardTitle, CardText, Row, Col } from 'reactstrap';
export default class Info extends Component {
	render() {
		const { countTodo } = this.props;
		return (
			<div>
				<Row>
					<Col sm={12}>
						<Card body>
							<CardTitle className="text-dark">Total TodoList</CardTitle>
							<CardText className="text-dark">{countTodo}</CardText>
						</Card>
					</Col>
				</Row>
			</div>
		);
	}
}
