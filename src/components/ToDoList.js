import React, { Component } from 'react';
import Api from '../Api';
import Warning from './Warning';
import Token from '../Token';
import {
	Col,
	Row,
	Modal,
	Input,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Button,
	InputGroup,
	InputGroupAddon,
	ButtonGroup,
	InputGroupText
} from 'reactstrap';

export default class ToDoList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			toDoList: [],
			modal: false,
			success: false,
			name: '',
			edited_id: '',
			edited_name: '',
			msg: ''
		};
	}

	handleChange = (e) => this.setState({ [e.target.name]: e.target.value, msg: '' });

	toggle = () =>
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));

	componentDidMount() {
		Api.post('/user/todoList/read', '', {
			headers: { Authorization: 'Bearer ' + this.props.token}
		})
			.then((res) => this.setState({ toDoList: res.data.todoList.reverse() }))
			.catch((err) => console.log(err.response));
	}

	addToDoList = () => {
		const { name } = this.state,
			data = { name };
		Api.post('/user/todoList/create', data, {
			headers: { Authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token')) }
		})
			.then((res) => {
				// console.log('add', res.data);
				this.setState({
					toDoList: [ ...this.state.toDoList, res.data.todoList ],
					msg: res.data.message,
					success: true,
					name: ''
				});
			})
			.catch((err) => console.log(err.response));
	};

	deleteToDo = (id) => {
		const data = { id };
		Api.delete('/user/todoList/delete', {
			headers: { Authorization: 'Bearer ' + Token },
			data
		})
			.then((res) => {
				this.setState({
					toDoList: res.data.user.todoList,
					success: false,
					msg: res.data.message
				});
			})
			.catch((err) => console.log('err_delete', err.response));
	};

	editToDo = (name, id) => {
		this.setState({ edited_name: name, edited_id: id });
		this.toggle();
	};

	doEdit = () => {
		const data = { name: this.state.edited_name, id: this.state.edited_id };
		Api.put('/user/todoList/update', data, {
			headers: { Authorization: 'Bearer ' + Token }
		})
			.then((res) => {
				console.log('res_edit', res);
				this.setState({
					toDoList: res.data.todoList,
					success: true,
					msg: res.data.message
				});
				this.toggle();
			})
			.catch((err) => console.log('err_delete', err.response));
	};

	render() {
		const { toDoList, msg, success } = this.state;
		return (
			<div className="mt-3">
				<Row>
					<Col sm={6} className="my-auto">
						<Col className="my-auto mx-5 text-center">
							<h1 className="text-light">Todo App</h1>
							{msg && <Warning color={success ? 'info' : 'danger'} msg={msg} />}
							<InputGroup>
								<Input
									className="input"
									name="name"
									onChange={this.handleChange}
									value={this.state.name}
								/>
								<Button color="info" onClick={this.addToDoList}>
									Add +
								</Button>
							</InputGroup>
						</Col>
					</Col>
					<Col sm={6} className="my-auto">
						{toDoList.map((todo, index) => {
							return (
								<Col sm={12} key={todo._id}>
									<InputGroup>
										<InputGroupAddon addonType="prepend">
											<InputGroupText>{index + 1}</InputGroupText>
										</InputGroupAddon>
										<Input value={todo.name} disabled />
										<ButtonGroup>
											<Button color="danger" onClick={() => this.deleteToDo(todo._id)}>
												Remove -
											</Button>
											<Button color="info" onClick={() => this.editToDo(todo.name, todo._id)}>
												Edit e
											</Button>
										</ButtonGroup>
									</InputGroup>
								</Col>
							);
						})}
					</Col>
				</Row>
				<Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader toggle={this.toggle}>Edit TodoList</ModalHeader>
					<ModalBody>
						<Input
							placeholder="nama todolist"
							name="edited_name"
							value={this.state.edited_name}
							onChange={this.handleChange}
						/>
						<Input
							style={{ display: 'none' }}
							name="edited_id"
							value={this.state.edited_id}
							onChange={() => {}}
						/>
					</ModalBody>
					<ModalFooter>
						<Button type="submit" color="primary" onClick={this.doEdit}>
							Edit e
						</Button>
						<Button color="secondary" onClick={this.toggle}>
							Cancel
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}
