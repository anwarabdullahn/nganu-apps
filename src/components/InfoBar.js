import React, { Component } from 'react';
import { Card, CardTitle, CardText, Button, Row, Col } from 'reactstrap';
import Token from '../Token';
import Api from '../Api';
import Info from './Info';
export default class InfoBar extends Component {
	constructor() {
		super();
		this.state = {
			name: '',
			email: ''
		};
	}

	componentDidMount() {
		Api.get('/user/me', {
			headers: { Authorization: 'Bearer ' + Token }
		})
			.then((res) => {
				console.log(res.data);
				this.setState({
					name: res.data.body.name,
					email: res.data.body.email
				});
			})
			.catch((err) => console.log(err.response));
	}

	render() {
		const { name, email } = this.state,
			{ countTodo } = this.props;
		return (
			<div>
				<Row>
					<Col sm={6}>
						<Info countTodo={countTodo} />
					</Col>
					<Col sm={6}>
						<Card body>
							<CardTitle className="text-dark">{name}</CardTitle>
							<CardText className="text-dark">{email}</CardText>
						</Card>
					</Col>
				</Row>
			</div>
		);
	}
}
