import React, { Component } from 'react';
import Token from '../Token';
import { Container, Row, Col } from 'reactstrap';
import Api from '../Api';
import ToDoList from '../components/ToDoList';
export default class Dashboard extends Component {
	constructor() {
		super();
		this.state = {
			id_todo_details: '',
			todolist: {}
		};
	}

	// getIdTodo = (id) => {
	// 	this.setState({ id_todo_details: id }, function() {
	// 		const data = { id: this.state.id_todo_details };
	// 		Api.post('/user/todoList/read', data, {
	// 			headers: { Authorization: 'Bearer ' + Token }
	// 		})
	// 			.then((res) => {
	// 				console.log('res satu', res.data.todoList);
	// 				this.setState({
	// 					todolist: res.data.todoList[0]
	// 				});
	// 			})
	// 			.catch((err) => console.log(err.response));
	// 	});
	// };

	render() {
		return (
			<Container fluid={true} className="main-background d-flex">
				{/* <Row>
					<Col sm={12} className="mt-3" />
				</Row> */}
				<Row className="m-auto w-100">
					<Col md={12}>
						<ToDoList token={Token} />
					</Col>
					{/* <Col md={6}>
						<ToDoDetails id={this.state.id_todo_details} todoList={this.state.todolist} />
					</Col> */}
				</Row>
			</Container>
		);
	}
}
