import React, { Component } from 'react';
import { Row, Col, Container } from 'reactstrap';
import { Route } from 'react-router-dom';
import '../assets/styles/_Main.scss';
import Landing from '../components/Landing';
import Login from '../components/Login';
import Register from '../components/Register';
class Main extends Component {
	render() {
		return (
			<Container fluid={true} className="d-flex main-background">
				<Row className="main m-auto">
					<Col md={6} className="m-auto greeting">
						<Landing />
					</Col>
					<Col md={6}>
						<Route path="/" component={Login} exact />
						<Route path="/register" component={Register} exact />
					</Col>
				</Row>
			</Container>
		);
	}
}

export default Main;
