import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import Main from './containers/Main';
import Dashboard from './containers/Dashboard';
import Token from './Token';

class App extends Component {
	render() {
		return (
			<Router>
				<Route path="/" component={Main} exact />
				<Route path="/register" component={Main} exact />
				<Route path="/dashboard" component={Dashboard} exact />
			</Router>
		);
	}
}

export default App;
